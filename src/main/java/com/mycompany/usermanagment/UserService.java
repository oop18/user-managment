/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    //mockup
    static {
        UserService.load();
        if (userList.isEmpty()) {
            userList.add(new User("admin", "password"));
        } else {
            if (!adminCheck()) {
                userList.add(new User("admin", "password"));
            }
    }
    }
    //create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }
    private static boolean adminCheck() {
        boolean check = false;
        for (User user : userList) {
            if (user.getUserName().equals("admin")) {
                check = true;
                return check;
            }
        }
        return check;
    }
    
    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    //update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    //read 1 user
    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        } //4
        return userList.get(index);
    }

    //read all user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    //search username
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return userList;
    }

    //delete user
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("User.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(UserService.getUsers());
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                fos.close();
            } catch (IOException ex) {

            }
        }
    }

    public static void load() {
        FileInputStream fis = null;
        try {
            File file = new File("User.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<User> temp = (ArrayList<User>) ois.readObject();
            UserService.addUser(temp);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        } finally {
            try {
                fis.close();
            } catch (IOException ex) {

            }
        }
    }

    private static void addUser(ArrayList<User> temp) {
        userList.addAll(temp);
    }
}
